export 'user_manager.dart';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:convert';

import 'package:routes_app_lib/widgets/request_location_access.dart';
import 'package:url_launcher/url_launcher.dart';

class Utilities {

  static copyToClipboard(String text){
    Clipboard.setData(ClipboardData(text: text));
  }
  static showToast(BuildContext context, String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 2,
        backgroundColor: Theme.of(context).primaryColor,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  static Map<String, dynamic> getJWTPayload(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      throw Exception('invalid token');
    }

    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw Exception('invalid payload');
    }

    return payloadMap;
  }

  static String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }

    return utf8.decode(base64Url.decode(output));
  }

  static showLocationPermissionDialog(BuildContext context)async{

    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.location);

    if (permission != PermissionStatus.granted){
      //show dialog
      Navigator.of(context).push(RequestLocationPermission());
    }
  }

  static openUrl(String url)async{
    if (await canLaunch(url)) {
    await launch(url);
    } else {
     print('Could not launch $url');
    }
  }

  static saveString(String key, String value) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<String> getString(String key)async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static pickPicture(BuildContext context, ValueChanged<File> pictureSelected){

    var getPicture =  (ImageSource source)async{
      var image = await ImagePicker.pickImage(source: source);
      pictureSelected(image);
    };
    showModalBottomSheet(context: context, builder: (context){
      return Container(
        child: new Wrap(
          children: <Widget>[
            new ListTile(
                leading: new Icon(Icons.photo_camera),
                title: new Text('Take a photo'),
                onTap: (){
                  getPicture(ImageSource.camera);
                }
            ),
            Divider(),
            new ListTile(
              leading: new Icon(Icons.photo_library),
              title: new Text('Choose from library'),
              onTap:  (){
                getPicture(ImageSource.gallery);
              },
            ),
          ],
        ),
      );
    });
  }

}