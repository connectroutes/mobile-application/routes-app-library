import 'package:shared_preferences/shared_preferences.dart';

class UserManager{
  static storeUserToken(String token)async{
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("user_token", token);
  }

  static isUserLoggedIn() async{
    var prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_token") != null;
  }
}