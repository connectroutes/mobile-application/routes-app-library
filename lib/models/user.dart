

class User {
  Map data;
  User(this.data);

  getPhoneNumber(){
    return data["phone_number"];
  }

  getEmailAddress(){
    return data["email_address"];
  }

  getName(){
    return data["name"];
  }
  getFirstName(){
    return data["name"].split(" ")[0];
  }

  bool isEmailVerified(){
    if (data["email_verified"] == null){
      return false;
    }
    return data["email_verified"];
  }
  String getReferrerCode(){
    if (data["referrer_code"] != null)
    return data["referrer_code"];
    return "ROUTES";
  }
  int getTotalRides(){
    if (data["total_rides"] != null)
      return data["total_rides"];
    return 0;
  }

  double getAverageRating(){
    if (data["average_rating"] != null)
      return data["average_rating"];
    return 0;
  }

  String getHomeAddress(){
    return getAddressDescription("home_address");
  }

  String getWorkAddress(){
    return getAddressDescription("work_address");
  }

  String getAddressDescription(String name){
    if(data[name]!= null){
      return data[name]["description"];
    }
    return null;
  }
}