import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddressTextField extends StatefulWidget {
  TextEditingController controller;
  String label, hint = '';
  bool disabled;
  FocusNode focusNode;
  AddressTextField(this.controller, this.label, this.hint,
      {this.disabled = false, this.focusNode});

  @override
  _AddressTextFieldState createState() => _AddressTextFieldState();
}

class _AddressTextFieldState extends State<AddressTextField> {
  bool isEmpty = true;

  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          widget.label,
          style: TextStyle(color: Color(0xffD8D8D9), fontSize: 12),
        ),
        new Padding(padding: EdgeInsets.only(top: 2)),
        Container(
          height: 56,
//          width: double.infinity,
//          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
//            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                  child: Container(
                height: 18,
                child: Stack(
                  children: <Widget>[
                    TextField(
                      focusNode: widget.focusNode,
                      enableInteractiveSelection: false,
                      enabled: !widget.disabled,
                      style: Theme.of(context).textTheme.body1,
                      controller: widget.controller,
                      decoration: new InputDecoration(
//                      border: InputBorder.none,
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffF6F6F6)),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffF6F6F6)),
                          ),
                          hintText: this.widget.hint,
                          suffixIcon: Container(
                            child: IconButton(
                              iconSize: 12,
                              onPressed: () {
                                print("taptdd");
                                if (!isEmpty) widget.controller.text = "";
                              },
                              icon: Icon(
                                isEmpty ? Icons.search : Icons.close,
                              ),
                            ),
                          ),
                          hintStyle: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context)
                                  .textTheme
                                  .body1
                                  .color
                                  .withOpacity(0.48))),
                    )
                  ],
                ),
              )),
            ],
          ),
        )
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      if (isEmpty != widget.controller.text.isEmpty) {
        setState(() {
          isEmpty = widget.controller.text.isEmpty;
        });
      }
    });
  }
}
