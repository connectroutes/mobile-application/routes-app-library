import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  String text;
  VoidCallback onPressed;
  bool disabled;
  bool loading;
  bool grey;
  bool outlined;
  Button(this.text, this.onPressed,
      {this.disabled = false,
      this.grey = false,
      this.loading = false,
      this.outlined = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ButtonTheme(
          minWidth: double.infinity,
          height: 60,
          child: outlined
              ? OutlineButton(
                  borderSide: BorderSide(color: Theme.of(context).primaryColor),
                  color:
                      grey ? Color(0xffD8D8D9) : Theme.of(context).primaryColor,
                  onPressed: disabled ? null : buttonPressed,
                  child: loading
                      ? CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        )
                      : Text(
                          this.text,
                          style: disabled
                              ? Theme.of(context).textTheme.body1
                              : TextStyle(
                                  color: grey ? Colors.black : Theme.of(context).primaryColor,
                                  fontSize: 14),
                        ),
                )
              : RaisedButton(
                  disabledColor: Color(0xffD8D8D9),
                  color:
                      grey ? Color(0xffD8D8D9) : Theme.of(context).primaryColor,
                  onPressed: disabled ? null : buttonPressed,
                  child: loading
                      ? CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        )
                      : Text(
                          this.text,
                          style: disabled
                              ? Theme.of(context).textTheme.body1
                              : TextStyle(
                                  color: grey ? Colors.black : Colors.white,
                                  fontSize: 14),
                        ),
                )),
    );
  }

  buttonPressed(){
    if (!loading){
      this.onPressed();
    }
  }
}
