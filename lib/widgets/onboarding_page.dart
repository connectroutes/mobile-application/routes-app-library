import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnboardingWidget extends StatefulWidget {
  List<Widget> introWidgetsList;
  String navigateTo;
  OnboardingWidget(this.introWidgetsList, this.navigateTo);
  @override
  _OnboardingViewState createState() => _OnboardingViewState();

  static Widget onboardingScreen(
      BuildContext context, String title, String asset, String description) {
    return Column(
      children: <Widget>[
        Column(
//          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            new Image.asset(
              asset,
              width: 285,
              height: 204,
            ),
            Container(
              margin: const EdgeInsets.only(top: 83.0),
              child: Text(title, style: Theme.of(context).textTheme.title),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20.0),
              child: Center(
                child: Text(
                  description,
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}

class _OnboardingViewState extends State<OnboardingWidget> {
  PageController controller = PageController();
  int currentPosition = 0;



  Widget circleBar(bool isActive, index) {
    var _kMaxZoom = 2.0;
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;

    if (isActive) {
      return AnimatedContainer(
        duration: Duration(milliseconds: 150),
        margin: EdgeInsets.symmetric(horizontal: 8),
        height: 11 * zoom,
        width: 11 * zoom,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
              width: 3, //
              color: Theme.of(context).primaryColor //    <--- border width here
          ),
        ),
      );
    }

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      height: 8,
      width: 8,
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.2),
        borderRadius: BorderRadius.circular(8.0),
      ),
    );
  }

  void getChangedPageAndMoveBar(int page) {
    setState(() {
      currentPosition = page;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: Container(
            margin: EdgeInsets.only(top: 100),
            padding: const EdgeInsets.only(left: 23.0, right: 17.0, bottom: 36),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: PageView.builder(
                    controller: controller,
                    physics: AlwaysScrollableScrollPhysics(),
                    itemCount: widget.introWidgetsList.length,
                    onPageChanged: (int page) {
                      getChangedPageAndMoveBar(page);
                    },
                    itemBuilder: (context, index) {
                      return widget.introWidgetsList[index];
                    },
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        InkWell(
                          child: Container(
                              width: 100,
                              height: 30,
                              child: Center(child: Text("Skip"))),
                          onTap: () {
                            Navigator.pushReplacementNamed(
                                context, widget.navigateTo);
                          },
                        ),
                        Container(
                          height: 11,
                          child: new DotsIndicator(
                            context,
                            controller: controller,
                            itemCount: widget.introWidgetsList.length,
                            onPageSelected: (int page) {
                              controller.animateToPage(
                                page,
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeInCirc,
                              );
                            },
                          ),
                        ),
                        InkWell(
                            onTap: () {
                              if (currentPosition == 2) {
                                Navigator.pushReplacementNamed(
                                    context, widget.navigateTo);
                              } else
                                controller.nextPage(
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.easeOut);
                            },
                            child: Container(
                              width: 100,
                              height: 30,
                              child: Center(
                                child: Text(
                                  "Next",
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .textTheme
                                          .title
                                          .color),
                                ),
                              ),
                            ))
                      ],
                    ),
                  ),
                )
              ],
            )));
  }
}

class DotsIndicator extends AnimatedWidget {
  DotsIndicator(
      this.context, {
        this.controller,
        this.itemCount,
        this.onPageSelected,
        this.color: Colors.blue,
      }) : super(listenable: controller);
  BuildContext context;

  /// The PageController that this DotsIndicator is representing.
  final PageController controller;

  /// The number of items managed by the PageController
  final int itemCount;

  /// Called when a dot is tapped
  final ValueChanged<int> onPageSelected;

  /// The color of the dots.
  ///
  /// Defaults to `Colors.white`.
  final Color color;
// The base size of the dots
  static const double _kDotSize = 8.0;
// The increase in the size of the selected dot
  static const double _kMaxZoom = 2.0;
// The distance between the center of each dot
  static const double _kDotSpacing = 25.0;
  Widget _buildDot(int index) {
    double selectedness = Curves.easeInCirc.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;

    /*if (controller.page == index){
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        width: _kDotSize * zoom,
        height: _kDotSize * zoom,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
              width: 3, //
              color: Theme.of(context).primaryColor//    <--- border width here
          ),
        ),
      );
    }

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      width: _kDotSize * zoom,
      height: _kDotSize * zoom,
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(0.2),
        borderRadius: BorderRadius.circular(8.0),
      ),
    );
*/

    return new Container(
      width: _kDotSpacing,
      child: new Center(
        child: new Material(
          color: Colors.white,
          type: MaterialType.circle,
          child: new Container(
            width:  controller.page == index ? 11 * zoom : 8 * zoom,
            height: controller.page == index ? 11 * zoom : 8 * zoom,
            child: new InkWell(
              onTap: () => onPageSelected(index),
            ),
            decoration: controller.page == index
                ? BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                  width: 3, //
                  color: Theme.of(context)
                      .primaryColor //    <--- border width here
              ),
            )
                : BoxDecoration(
              color: Colors.black.withOpacity(0.2),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: new List<Widget>.generate(itemCount, _buildDot),
    );
  }
}
