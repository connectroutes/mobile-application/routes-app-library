
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:routes_app_lib/routes_app_lib.dart';

class ProfilePicture extends StatelessWidget{
  double size;
  ProfilePicture({this.size = 70});
  var userService = GetIt.instance<UserService>();

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
        radius: size,
        backgroundColor: Color(0xffD8D8D9),
        backgroundImage: AssetImage("assets/user-avatar.png")
    );
  }
}