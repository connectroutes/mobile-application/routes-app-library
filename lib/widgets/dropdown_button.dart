import 'package:flutter/material.dart';

class RoutesDropdownButton extends StatefulWidget {
  List<String> items;
  String label;
  String hint;
  String value;
  RoutesDropdownButton(this.items, this.label, this.hint, {this.value});

  @override
  State createState() {
    return _DropDownButtonState();
  }
}

class _DropDownButtonState extends State<RoutesDropdownButton> {
  String value;

  @override
  Widget build(BuildContext context) {
    var deviceWidth = MediaQuery.of(context).size.width;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Text(
          widget.label,
          style: TextStyle(fontWeight: FontWeight.w500),
        ),
        new Padding(padding: EdgeInsets.only(top: 12)),
        Container(
          decoration: BoxDecoration(
              border: Border.all(width: 1, color: Color(0xE5E5E5CC))),
          width: double.infinity,
          child: new DropdownButton<String>(
            value: value == null ? widget.value : value,
            hint: Container(
              width: deviceWidth - 50,
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(widget.hint),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
            underline: Container(),
            icon: Container(),
            items: widget.items.map((String value) {
              return new DropdownMenuItem<String>(
                value: value,
                child: Container(
                  width: deviceWidth - 50,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Text(value),
                      Icon(Icons.arrow_drop_down)
                    ],
                  ),
                ),
              );
            }).toList(),
            onChanged: (newValue) {
              setState(() {
                value = newValue;
              });
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[],
        ),
      ],
    );
  }
}
