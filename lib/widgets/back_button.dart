import 'package:flutter/material.dart';

class BackButton extends StatelessWidget {
  BackButton();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: Icon(Icons.arrow_back),
        height: 25,
        width: 25,
      ),
      onTap: () {
        Navigator.pop(context);
      },
    );
  }
}
