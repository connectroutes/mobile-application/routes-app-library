import 'package:flutter/material.dart';

class TabSelectionButton extends StatefulWidget {
  List<String> hints;
  String label;
  ValueChanged<String> valueChanged;
  TabSelectionButton(this.hints, this.label, this.valueChanged);

  @override
  State createState() {
    return _TabSelectionButtonState();
  }
}

class _TabSelectionButtonState extends State<TabSelectionButton> {

  String selected = "";

  @override
  Widget build(BuildContext context) {

    if (selected != widget.hints[1])
      selected = widget.hints[0];

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          widget.label,
          style: TextStyle(fontWeight: FontWeight.w500),
        ),
        new Padding(padding: EdgeInsets.only(top: 12)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[

            _buildButton(widget.hints[0]),
            _buildButton(widget.hints[1]),
          ],
        ),
      ],
    );
  }

  _buildButton(String name){
    var deviceWidth = MediaQuery.of(context).size.width;

    var buttonWidth = (deviceWidth - 44) / 2;

    if (selected == name){
      return GestureDetector(
        child: Container(
            width: buttonWidth,
            height: 30,
            child:  FlatButton(
              color: Theme.of(context).primaryColor,
              onPressed: (){
                setState(() {
                  selected = name;
                  widget.valueChanged(name);
                });
              },
              child: Text(name, style: TextStyle(color: Colors.white),),
            )),
      );
    }

    return Container(
      width: buttonWidth,
      height: 30,
      decoration: BoxDecoration(
        border: Border(
          right: BorderSide( //                   <--- left side
            color: Colors.grey,
            width: 1.0,
          ),
          left: BorderSide( //                   <--- left side
            color: Colors.grey,
            width: 1.0,
          ),
          top: BorderSide( //                    <--- top side
            color: Colors.grey,
            width: 1.0,
          ),
          bottom: BorderSide( //                    <--- top side
            color: Colors.grey,
            width: 1.0,
          ),
        ), // se
      ),
      child: FlatButton(
        color: Colors.white,
        shape: RoundedRectangleBorder(),
        onPressed: (){
          setState(() {
            selected = name;
            widget.valueChanged(name);
          });
        },
        child: Text(name),
      ),
    );
  }


}
