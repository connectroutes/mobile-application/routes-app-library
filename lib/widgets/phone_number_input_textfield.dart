
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PhoneNumberInputTextField extends StatefulWidget{

  TextEditingController controller;
  bool disableFocusColor;
  String hint;
  Widget suffix;
  PhoneNumberInputTextField(this.controller, {this.disableFocusColor = false, this.hint="Enter your mobile number", this.suffix});

  @override
  _PhoneNumberInputTextFieldState createState() => _PhoneNumberInputTextFieldState();
}

class _PhoneNumberInputTextFieldState extends State<PhoneNumberInputTextField> {
  final FocusNode _focusNode = FocusNode();

  bool focused = false;
  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus){
        setState(() {
          focused = true;
        });
      } else {
        focused = false;
      }
    });
  }
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      width: double.infinity,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        border: Border.all(
            width: 1, //
            color: focused && !widget.disableFocusColor ? Theme.of(context).primaryColor : Color(0xE5E5E5CC)//    <--- border width here
        ),
        borderRadius: BorderRadius.circular(1.0),
      ),
        child: Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 18, left: 12),
                child: Image(image: AssetImage("assets/nigeria_flag.png", package: "routes_app_lib"), width: 27, height: 14,),
              ),
              Container(
                margin: EdgeInsets.only(left: 7.33, top: 20, bottom: 18,),
                child: Text("+234"),
              ),
              Container(
                width: 1,
                margin: EdgeInsets.only(left: 11.67),
                height: double.maxFinite,
                color: focused && !widget.disableFocusColor ? Theme.of(context).primaryColor : Color(0xffF6F6F6),
                child: VerticalDivider(),

              ),
              Expanded(child: Container(
                margin: EdgeInsets.only(left: 12, top: 20,),
                height: 18,
                child: TextField(
                  focusNode: _focusNode,
                  keyboardType: TextInputType.phone,
                  maxLength: 12,
                  style: Theme.of(context).textTheme.body1,
                  controller: widget.controller,
                  decoration: new InputDecoration(
                      suffix: widget.suffix,
                      border: InputBorder.none,
                      hintText: widget.hint,
                      counterText: "",
                      hintStyle: TextStyle(fontSize: 14, color: Theme.of(context).textTheme.body1.color.withOpacity(0.46))
                  ),
                ),
              )),
            ],
          ),
        ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
  }


}

