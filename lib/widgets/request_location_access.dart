import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:routes_app_lib/routes_app_lib.dart';

class RequestLocationPermission extends ModalRoute<void> {
  @override
  Duration get transitionDuration => Duration(milliseconds: 500);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.white.withOpacity(0.4);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      ) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
      Padding(padding: EdgeInsets.only(top: 50),),
       RippleAnimation(),
        Padding(padding: EdgeInsets.only(top: 50),),
        Text(
          'Allow Routes access your location?',
          style: TextStyle(color: Colors.black,),
        ),
        Padding(padding: EdgeInsets.only(top: 20),),
        Padding(
          padding: const EdgeInsets.all(19.0),
          child: Button("Yes, access my location", ()async{
            Map<PermissionGroup, PermissionStatus> permissions = await PermissionHandler().requestPermissions([PermissionGroup.location]);
            PermissionStatus status = permissions[PermissionGroup.location];
            if (status == PermissionStatus.granted){
              Navigator.pop(context);
            }

          }),
        ),
        Padding(
          padding: const EdgeInsets.all(19.0),
          child: Button("No, do not access my location", (){
            Navigator.pop(context);
          }, grey: true,),
        ),
      ],
    );
  }

  @override
  Widget buildTransitions(
      BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    // You can add your own animations for the overlay content
    return FadeTransition(
      opacity: animation,
      child: ScaleTransition(
        scale: animation,
        child: child,
      ),
    );
  }
}