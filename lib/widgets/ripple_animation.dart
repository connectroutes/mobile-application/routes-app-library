import 'dart:math';

import 'package:flutter/material.dart';

class RippleAnimation extends StatefulWidget{


  @override
  _RippleAnimationState createState() => _RippleAnimationState();
}

class _RippleAnimationState extends State<RippleAnimation> with SingleTickerProviderStateMixin  {
  AnimationController _animationController;


  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  void initState() {
    super.initState();
    _animationController = new AnimationController(
      vsync: this,
    );
  }

  void _startAnimation() {
    _animationController.stop();
    _animationController.reset();
    _animationController.repeat(
      period: Duration(seconds: 1),
    );
  }


  @override
  Widget build(BuildContext context) {
    _startAnimation();
    return CustomPaint(
      painter: RipplePainter(_animationController, context),
      child: Column(
        children: <Widget>[
          new SizedBox(
            width: 200.0,
            height: 200.0,
          ),
        ],
      ),
    );
  }

}
class RipplePainter extends CustomPainter {
  final Animation<double> _animation;
  BuildContext context;

  RipplePainter(this._animation, this.context) : super(repaint: _animation);

  void circle(Canvas canvas, Rect rect, double value) {
    double opacity = (1.0 - (value / 4.0)).clamp(0.0, 1.0);
    Color color = new Color.fromRGBO(74, 114, 164, opacity);
//    Color color = Theme.of(context).primaryColor;

    double size = rect.width / 2;
    double area = size * size;
    double radius = sqrt(area * value / 4);

    final Paint paint = new Paint()..color = color;
    canvas.drawCircle(rect.center, radius, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = new Rect.fromLTRB(0.0, 0.0, size.width, size.height);

    for (int wave = 3; wave >= 0; wave--) {
      circle(canvas, rect, wave + _animation.value);
    }
  }


  @override
  bool shouldRepaint(RipplePainter oldDelegate) {
    return true;
  }
}
