import 'dart:async';

import 'package:flutter/material.dart';

class TextCountDown extends StatefulWidget{
  int seconds;
  VoidCallback onCountDownToZero;
  TextCountDown(this.seconds, this.onCountDownToZero);

  @override
  _TextCountDownState createState() => _TextCountDownState();
}

class _TextCountDownState extends State<TextCountDown> {

  int seconds;

  Timer timer;
  @override
  Widget build(BuildContext context) {
    return Text("$seconds seconds", style: TextStyle(fontWeight: FontWeight.bold),);
  }

  @override
  void initState() {
    super.initState();
    seconds = widget.seconds;
    timer = new Timer.periodic(Duration(seconds: 1), (Timer t){
      if (seconds == 0){
        timer.cancel();
        widget.onCountDownToZero();
      } else {
        setState(() {
          seconds--;
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    timer?.cancel();
  }


}