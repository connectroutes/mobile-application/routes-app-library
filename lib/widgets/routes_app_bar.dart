
import 'package:flutter/material.dart';

class RoutesAppBar extends StatelessWidget{

  String title;
  Widget rightWidget;
  RoutesAppBar(this.title, {this.rightWidget});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
              Navigator.pop(context);
            },),
            Padding(padding: EdgeInsets.only(left: 20),),
            Text(this.title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
          ],
        ),
        rightWidget == null ? Container() : rightWidget
      ],
    );
  }
}