import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoutesTextField extends StatelessWidget {
  TextEditingController controller;
  String label, hint = '';
  TextInputType textInputType = TextInputType.text;
  int maxLength;
  bool obscureText;
  bool capitalizeWords;
  double height;
  bool dottedBorder;
  TextAlign textAlign;
  FontWeight hintFontWeight;
  TextStyle textStyle;
  bool loading;
  bool readOnly;
  String errorText;
  Widget suffix;

  RoutesTextField(this.controller,
      {this.label,
      this.hint,
      this.textInputType,
      this.maxLength,
      this.obscureText = false,
      this.capitalizeWords = false,
      this.height = 56,
      this.dottedBorder = false,
      this.loading = false,
      this.errorText,
      this.textStyle,
      this.suffix,
      this.readOnly = false,
      this.textAlign = TextAlign.left,
      this.hintFontWeight = FontWeight.normal});
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        this.label == null
            ? Container()
            : Text(
                label,
                style: TextStyle(fontWeight: FontWeight.w500),
              ),
        new Padding(padding: EdgeInsets.only(top: 12)),
        buildTextField(context),
        ...buildErrorText(),
      ],
    );
  }

  List<Widget> buildErrorText() {
    if (errorText == null) {
      return [];
    } else {
      return [
        new Padding(padding: EdgeInsets.only(top: 12)),
        Text(
          errorText,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 11,
              color: Color(0xffD63C3C)),
        )
      ];
    }
  }

  Widget buildTextField(BuildContext context) {
    var textContainer = Container(
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
              child: Container(
            margin: EdgeInsets.only(
              left: 12,
              top: height / 5,
            ),
            height: 18,
            child: TextField(
              readOnly: readOnly,
              textAlign: textAlign,
              keyboardType: this.textInputType,
              maxLength: this.maxLength,
              obscureText: this.obscureText,
              textCapitalization: this.capitalizeWords
                  ? TextCapitalization.words
                  : TextCapitalization.none,
              style: textStyle == null
                  ? Theme.of(context).textTheme.body1
                  : textStyle,
              controller: controller,
              decoration: new InputDecoration(
                  border: InputBorder.none,
                  hintText: this.hint,
                  counterText: "",
                  suffix: suffix != null
                      ? suffix
                      : loading
                          ? Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: SizedBox(
                                child: CircularProgressIndicator(),
                                width: 14,
                                height: 14,
                              ),
                            )
                          : null,
                  hintStyle: TextStyle(
                      fontSize: 14,
                      fontWeight: hintFontWeight,
                      color: Theme.of(context)
                          .textTheme
                          .body1
                          .color
                          .withOpacity(0.46))),
            ),
          )),
        ],
      ),
    );
    if (dottedBorder) {
      return DottedBorder(
        color: Color(errorText != null ? 0xffD63C3C : 0xE5E5E5CC),
        strokeWidth: 1,
        dashPattern: [7, 7],
        child: Container(
          child: textContainer,
          height: height,
        ),
      );
    } else {
      return Container(
        height: height,
        width: double.infinity,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(width: 1, color: Color(0xE5E5E5CC)),
          borderRadius: BorderRadius.circular(1.0),
        ),
        child: textContainer,
      );
    }
  }
}
