
import 'package:flutter/material.dart';

class AddressCircles extends StatelessWidget{

  int noOfSmallCircles;
  static const double smallSize = 3;
  static const double bigSize = 7.87;
  AddressCircles(this.noOfSmallCircles);

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        _drawCircle(Theme.of(context).primaryColor, 8),
        ..._generateSmallCircles(),
        _drawCircle(Theme.of(context).accentColor, 8),

      ],
    );
  }

  List<Widget>_generateSmallCircles(){
    List<Widget> circles = [];
    for (int i = 0; i<noOfSmallCircles; i++){
      circles.add(_drawCircle(Color(0xFFEFEFEF), smallSize));
    }
    return circles;
  }

  _drawCircle(Color color, double size) {
    return Container(
      margin: EdgeInsets.only(top: 2, bottom: 2, right: 2),
      width: size,
      height: size,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(size),
      ),
    );
  }
}