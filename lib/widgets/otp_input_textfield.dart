
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OtpInputBox extends StatelessWidget{
  ValueChanged<String> valueChanged;
  FocusNode focusNode;
  FocusNode nextNode;
  String text = "";
  TextEditingController _controller = TextEditingController();


  OtpInputBox(this.valueChanged, this.focusNode, this.nextNode, this.text);

  @override
  Widget build(BuildContext context) {
    _controller.text = text;
    return Container(
      height: 60,
      width: 59.68,
      child: TextField(
        controller: _controller,
//        maxLength: 1,
        focusNode: focusNode,
        keyboardType: TextInputType.number,
        showCursor: false,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30, color: Theme.of(context).textTheme.body1.color),
        decoration: new InputDecoration(
//            border: OutlineInputBorder(),
          hintText: '',
          counterText: "",
        ),
        onChanged: (text){
          if (text.length == 1){
            if (nextNode != null){
              FocusScope.of(context).requestFocus(nextNode);
            }
            this.valueChanged(text);
          } else {
            this.valueChanged(text);
          }
        },
      ),
    );
  }

}

class OtpInputTextField extends StatefulWidget{
  ValueChanged<String> valueChanged;
  OtpInputTextField(this.valueChanged);

  @override
  State createState() {
    return OtpInputTextFieldState();
  }



}

class OtpInputTextFieldState extends State<OtpInputTextField>{
  List<String> inputs = ["", "", "", ""];
  List<FocusNode> focusNodes = [];

  OtpInputTextFieldState();


  @override
  void initState() {
    super.initState();
    for (int i = 0; i<=3; i++){
      focusNodes.add(new FocusNode());
    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      height: 60,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          OtpInputBox((value){

            if (value.length == 4){
              setState(() {
                inputs[0] = value[0];
                inputs[1] = value[1];
                inputs[2] = value[2];
                inputs[3] = value[3];
              });
              widget.valueChanged(value);
            } else {
              inputs[0] = value;
            }
          }, focusNodes[0], focusNodes[1], inputs[0]),
          OtpInputBox((value){
            inputs[1] = value;
          }, focusNodes[1], focusNodes[2], inputs[1]),
          OtpInputBox((value){
            inputs[2] = value;
          }, focusNodes[2], focusNodes[3], inputs[2]),
          OtpInputBox((value){
            inputs[3] = value;
            widget.valueChanged(inputs.join());
          }, focusNodes[3], null, inputs[3]),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    for (var node in focusNodes){
      node.dispose();
    }
  }


}


