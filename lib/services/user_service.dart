import 'dart:convert';

import 'package:get_it/get_it.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserService{

  bool isReady = false;
  String userToken;
  User user;

  SharedPreferences prefs;
  UserService(){}

  var locationService = GetIt.instance<LocationService>();

  Future<User> loadUser()async{
    prefs = await SharedPreferences.getInstance();
    print("load user");
    print( prefs.getString("user_token"));
    print( prefs.getString("user_data"));

    userToken = prefs.getString("user_token");
    isReady = true;
    if (userToken == null){
      return null;
    }
    user = new User(json.decode(prefs.getString("user_data")));
    locationService.getCurrentLocation();
   return user;
  }

  User getCurrentUser(){
    return user;
  }

  String getSessionToken(){
    return userToken;
  }

  storeUserToken(String token, String user_data) async{
    prefs.setString("user_token", token);
    prefs.setString("user_data", user_data);
    userToken = token;
    user =  new User(json.decode(user_data));
  }

  deleteUserToken() async{
    await prefs.remove("user_data");
    await prefs.remove("user_token");
  }
}
