

import 'package:geolocator/geolocator.dart';
import 'package:routes_app_lib/models/location.dart';

class LocationService{

  Location location;

  Future<Location> getCurrentLocation({bool cached = false}) async{
    if (cached && location!=null){
      return location;
    }
    Position position = await Geolocator().getCurrentPosition();
    if (position != null){
      location = new Location(position.latitude, position.longitude);
      return location;
    }
    return null;
  }

}
